import React from 'react';
import { Redirect } from 'react-router-dom';
import CountDown from './CountDown';
import StartBtn from './StartBtn';
import socket from '../socketConfig';
import DisplayWords from './DisplayWords';
import Form from './Form';
import ProgressBar from './ProgressBar';
import ScoreBoard from './ScoreBoard';
import DisplayGameCode from './DisplayGameCode';

const findPlayer = (players) => {
    return players.find((player) => player.socketID === socket.id);
};

const TypeRacer = ({ gameState }) => {
    const { _id, players, words, isOver, isOpen } = gameState;
    const player = findPlayer(players);
    if (_id === '') return <Redirect to="/"></Redirect>;
    return (
        <div className="text-center">
            <DisplayWords words={words} player={player}></DisplayWords>
            <ProgressBar players={players} player={player} wordsLength={words.length}></ProgressBar>
            <Form isOpen={isOpen} isOver={isOver} gameID={_id}></Form>
            <CountDown></CountDown>
            <StartBtn player={player} gameID={_id}></StartBtn>
            <DisplayGameCode gameID={_id}></DisplayGameCode>
            <ScoreBoard players={players}></ScoreBoard>
        </div>
    );
};

export default TypeRacer;
